FROM alpine@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ADD trigger /usr/local/bin/

RUN apk add curl

RUN curl -Lo /usr/local/bin/kubectl https://dl.k8s.io/release/v1.23.0/bin/linux/amd64/kubectl

RUN chmod +x /usr/local/bin/kubectl

ENTRYPOINT /usr/local/bin/trigger
